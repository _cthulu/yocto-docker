FROM ubuntu:18.04

WORKDIR /yocto

RUN apt-get update 
RUN apt-get upgrade

RUN apt-get install -yq locales
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && locale-gen

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get install -yq tzdata && \
    ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata

RUN apt-get install -yq gawk wget git diffstat unzip texinfo gcc build-essential chrpath \
    socat cpio python3 python3-pip python3-pexpect xz-utils debianutils iputils-ping vim sudo \
    python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 xterm python3-subunit mesa-common-dev zstd liblz4-tool

RUN apt-get install -yq tmux qemu-system-x86-64

RUN useradd -rm -d /home/build -s /bin/bash -g root -G sudo -u 1000 build
RUN mkdir -p /yocto  && chown build /yocto

VOLUME /yocto

USER build

CMD /bin/bash
